# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('FizzBuzzApp', '0003_auto_20150522_1751'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fizzbuzz',
            name='fizzbuzz_id',
        ),
    ]
