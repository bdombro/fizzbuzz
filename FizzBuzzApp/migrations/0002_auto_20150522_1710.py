# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('FizzBuzzApp', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='fizzbuzz',
            options={'ordering': ('creation_date',)},
        ),
    ]
