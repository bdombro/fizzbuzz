# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('FizzBuzzApp', '0002_auto_20150522_1710'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fizzbuzz',
            name='fizzbuzzcollections',
        ),
        migrations.AlterField(
            model_name='fizzbuzz',
            name='useragent',
            field=models.CharField(max_length=20),
        ),
        migrations.DeleteModel(
            name='FizzBuzzCollection',
        ),
    ]
