# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('FizzBuzzApp', '0004_remove_fizzbuzz_fizzbuzz_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fizzbuzz',
            name='useragent',
            field=models.CharField(max_length=200),
        ),
    ]
