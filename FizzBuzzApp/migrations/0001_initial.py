# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FizzBuzz',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('fizzbuzz_id', models.CharField(max_length=20)),
                ('creation_date', models.DateTimeField(verbose_name='date published')),
                ('message', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='FizzBuzzCollection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('message', models.CharField(max_length=200)),
            ],
        ),
        migrations.AddField(
            model_name='fizzbuzz',
            name='fizzbuzzcollections',
            field=models.ForeignKey(to='FizzBuzzApp.FizzBuzzCollection'),
        ),
        migrations.AddField(
            model_name='fizzbuzz',
            name='useragent',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
