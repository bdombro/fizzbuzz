from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from FizzBuzzApp.models import FizzBuzz
from FizzBuzzApp.serializers import UserSerializer, GroupSerializer, FizzBuzzSerializer

from rest_framework.response import Response
from rest_framework import status

# from django.http import HttpRequest


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class FizzBuzzViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows FizzBuzzes to be viewed or edited.
    """
    queryset = FizzBuzz.objects.all()
    serializer_class = FizzBuzzSerializer



