from django.contrib.auth.models import User, Group
from rest_framework import serializers
from FizzBuzzApp.models import FizzBuzz


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class CurrentUserAgentDefault(object):
    """
    This class will retrieve the user agent for serializers.  It is based on
    fields::CurrentUserDefault
    """
    def set_context(self, serializer_field):
        self.useragent = serializer_field.context['request'].META['HTTP_USER_AGENT']

    def __call__(self):
        return self.useragent

    def __repr__(self):
        return unicode_to_repr('%s()' % self.__class__.__name__)


class FizzBuzzSerializer(serializers.HyperlinkedModelSerializer):
    fizzbuzz_id = serializers.CharField(source='id', read_only=True)
    useragent = serializers.CharField(max_length=200,read_only=True,default=CurrentUserAgentDefault())
    
    class Meta:
        model = FizzBuzz
        fields = ('fizzbuzz_id','creation_date','useragent','message')
        # read_only_fields = ('useragent',) # useful if want to add read_only to a field




