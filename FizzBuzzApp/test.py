from datetime import datetime
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from FizzBuzzApp import views

class FizzBuzzTests(APITestCase):
    
    # FizzBuzz Test Variables
    message = 'Hi!'
    useragent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36'

    
    def test_create_fizzbuzz(self):
        """
        Ensure we can create a new account object.
        """
        
        # Post to server
        response = self.client.post('/fizzbuzz', {'message': self.message}, format='json', HTTP_USER_AGENT=self.useragent)
        
        # Validate success
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Validate response contents
        self.validate_response(response)

        return response.data['fizzbuzz_id']

    
    def test_retrieve_fizzbuzz(self):
        """
        Ensure we can retrieve a single fizzbuzz object.
        """
        
        # Ensure a fizzbuzz exists
        fizzbuzz_id = self.test_create_fizzbuzz()

        # Get from server
        response = self.client.get('/fizzbuzz/'+fizzbuzz_id)
        
        # Validate success
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Validate response contents
        self.validate_response(response)


    def test_fizzbuzz_list(self):
        """
        Ensure we can retrieve a list of fizzbuzz objects.
        """
        
        # Ensure 2 fizzbuzz exist
        fizzbuzz_id1 = self.test_create_fizzbuzz()
        fizzbuzz_id2 = self.test_create_fizzbuzz()

        # Get from server
        response = self.client.get('/fizzbuzz')

        # Validate success
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Validate response contents
        self.validate_response(response,'list')


    def validate_response(self, response, type='single'):
        if type=='list':
            response.data = response.data[1]

        # Validate fizzbuzz_id
        if 'fizzbuzz_id' not in response.data:
            raise KeyError("fizzbuzz_id not in response")
        if not response.data['fizzbuzz_id'].isdigit():
            raise ValueError("fizzbuzz_id is invalid: "+response.data['fizzbuzz_id'])

        # Validate creation_date is set and valid
        if 'creation_date' not in response.data:
            raise KeyError("creation_date not in response")
        try:
            datetime.strptime(response.data['creation_date'],"%Y-%m-%dT%H:%M:%S.%fZ")
        except ValueError:
            raise ValueError("creation_date is invalid: "+response.data['creation_date'])

        # Validate useragent
        if 'useragent' not in response.data:
            raise KeyError("useragent not in response")
        self.assertEqual(response.data['useragent'],self.useragent)
        
        # Validate message
        if 'message' not in response.data:
            raise KeyError("message not in response")
        self.assertEqual(response.data['message'],self.message)
        
        

    