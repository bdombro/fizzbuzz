from django.db import models

class FizzBuzz(models.Model):
    useragent = models.CharField(max_length=200)
    creation_date = models.DateTimeField(auto_now_add=True)
    message = models.CharField(max_length=200)

    class Meta:
    		ordering = ('creation_date',)