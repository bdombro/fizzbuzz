FizzBuzz
====================

Objective
---------------------

The task is to create an API endpoint for “fizzbuzzes” that supports 3 operations: retrieve, list, and create.  
GET /fizzbuzz/123    to retrieve a single fizz buzz object  
GET /fizzbuzz            to list all fizzbuzz objects  
POST /fizzbuzz         to create a new fizzbuzz object  

I’ve created an API doc or you here:  [http://docs.fizzbuzz.apiary.io](http://docs.fizzbuzz.apiary.io)


Please utilize Django REST framework ([http://www.django-rest-framework.org/](http://www.django-rest-framework.org/)).  When you’ve completed the the code, please upload to a public git repo and send me the link.

Notes:  Don’t worry about setting up a postgres db, just use the stock sqllite dev database that django uses by default.  To test the endpoint I will run your code locally, don’t worry about hosting or anything like that.


Testing
---------------------

This app has a built in test in test.py, which can be run by cli:
```
python manage.py test FizzBuzzApp
```



### Credits

Development  
Brian Dombrowski @ Benevolent Web L.L.C.  
The Django developer community

Tasking  
John @ Mariana Tek